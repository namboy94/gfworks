# GFWorks

Gfworks offers python developers a simple way to implement a GUI in different graphical frameworks like Gtk or Tkinter
with a single implementation. This framework is very much a work in progress and the class and method calls are
subject to change.

## Using a template

To use a template, you will need to create a class that inherits from one of the implemented templates.

This class needs to override the __init__ method while also calling the super().__init__ method.

Furthermore, the lay_out method needs to be implemented to add widgets and other UI elements to the GUI.

## Links:

This project is mirrored to:

* [github.com](https://github.com/namboy94/gfworks)
* [gitlab.com](https://gitlab.com/namboy94/gfworks)
* [bitbucket.org](https://bitbucket.org/namboy94/gfworks)

[//]: # ([Documentation](http://gitlab.namibsun.net/namboy94/gfworks/wikis/git_stats/general.html))
[//]: # ([Documentation](http://gitlab.namibsun.net/namboy94/gfworks/wikis/html/index.html))

Check out this project's [python package index site](https://pypi.python.org/pypi/gfworks)!